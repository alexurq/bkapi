from flask import Flask
from flask_restful import Resource, Api
from resources.home import Home
from resources.zonesList import ZonesList
from resources.zones import Zones
from resources.zonesv2 import Zones as Zonesv2
from resources.units import Unit
import common.zones  #init zones

app = Flask(__name__)
api = Api(app)

api.add_resource(Home, '/rest/v1/')
api.add_resource(ZonesList, '/rest/v1/zones', endpoint='allzones')
api.add_resource(Zones, '/rest/v1/zones/<zone_id>', endpoint='zonev1')
api.add_resource(Zonesv2, '/rest/v1/zonesv2/<zone_id>', endpoint='zonev2')
api.add_resource(Unit, '/rest/v1/units/<unit_id>', endpoint='unit')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
