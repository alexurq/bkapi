import common.bkct600 as bkct600


class oldzone(object):
	def __init__(self, zoneid, name, currentInput, powerStatus):
		self.zoneid = zoneid
		self.name = name
		self.currentInput = currentInput
		self.powerStatus = powerStatus

class Zone(object):
	def __init__(self, id):
		self.id = id

	def getPowerStatus(self):
		"""Returns Power Status of Zone: On/Off !not available"""
		return bkct600.BK600(self.id, "powerstatus")
		# return 'error'
	def setInput(self, input):
		"""Sets the input of the zone"""
		result = bkct600.BK600(self.id, "input", input)
		return result
	def setPower(self, value):
		"""Sets the Power of the Zone"""
		return bkct600.BK600(self.id, "power", value)
	def setVolume(self, value):
		"""Sets volume of zone"""
		return bkct600.BK600(self.id, "volume", value)

zone1 = oldzone('1','backyard', 'input1', 'on')
zone2 = oldzone('2','courtyard', 'input1', 'off')
zones = [zone1,zone2]
zonesObj = {"zone1": zone1, "zone2":zone2}
