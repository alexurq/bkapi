import socket
import sys
import select
import common.config as config


class Unit(object):
    def __init__(self, id):
        self.id = id
        # self.codeSetID = getattr(config, id) -- for zones
        self.codeSetID = id
        self.HOST = config.HOST
        self.PORT = config.PORT
        # self.connect()

    def connect(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(10)
        self.s.connect((self.HOST, self.PORT))
        """Clear the Banner"""
        banner_data = self.s.recv(50)

    def disconnect(self):
        self.s.close()

    # def readFunction(self):
    #     amount_received = 0
    #     amount_expected = 150

    #     while amount_received < amount_expected:
    #         data = self.s.recv(150)
    #         amount_received += len(data)
    #         # print >>sys.stderr, 'received {0}'.format(data)
    #         result = '{0}'.format(data)
    #         print("data received:{0}".format(data))
    #     return data

    def sendCommand(self, command):
        result = ""

        def emptySocketBuffer():
            self.s.settimeout(0.0)
            while 1:
                try:
                    data = self.s.recv(150)
                except:
                    break
            self.s.settimeout(10)

        def parseResult(res):
            # because of the read function decoded the data already
            # val = res.decode('UTF-8').split(',')[3]
            val = res.split(',')[3]
            index = val.find('=')
            return val[index+1:index+3]

        def readFunction():
            amount_received = 0
            amount_expected = 150

            # # while amount_received < amount_expected:
            # data = self.s.recv(50)
            # # amount_received += len(data)
            # # print >>sys.stderr, 'received {0}'.format(data)
            # result = '{0}'.format(data)
            # return data
            End = ')'
            total_data = []
            data = ''
            while True:
                data = self.s.recv(8192).decode('UTF-8')
                if End in data:
                    total_data.append(data[:data.find(End)])
                    break
                total_data.append(data)
                if len(total_data) > 1:
                    # check if end_of_data was split
                    last_pair = total_data[-2]+total_data[-1]
                    if End in last_pair:
                        total_data[-2] = last_pair[:last_pair.find(End)]
                        total_data.pop()
                        break
            return ''.join(total_data)

        def writeFunction(command):
            print("sending {0}".format(command))
            self.s.send(command.encode())
            return parseResult(readFunction())

        # clear buffer
        # emptySocketBuffer()
        return writeFunction(command)

    def getInfo(self):
        """Returns Power Status of Unit: On/Off !not available"""
        results = {}
        try:
            self.connect()
            powerResult = self.sendCommand("(00,G, Z{0}, 24;)".format(
                                           self.codeSetID))
            if powerResult == '01':
                results['powerStatus'] = 'On'
            else:
                results['powerStatus'] = 'Off'
            results['unit'] = self.id
            # results['codeSetID'] = self.codeSetID
            # # resultInput = self.sendCommand("(00,G,P{0}=FF,02;)".format(self.codeSetID))
            # # results['currentInput'] = getattr(config, "pp{0}".format(resultInput))
            # results['currentInput'] = getattr(
            #         config, "pp{0}".format(
            #             self.sendCommand("(00,G,P{0}=FF,02;)".format(
            #                              self.codeSetID))))
            # volumeResult = self.sendCommand("(00,G,P{0}=FF,01;)".format(
            #                                 self.codeSetID))
            # if volumeResult == '00':
            #     results['volume'] = 'MUTE'
            # elif volumeResult == '28':
            #     results['volume'] = 'MAX'
            # else:
            #     results['volume'] = 'In Between'
            # results['name'] = self.sendCommand(
            #     "(00,G,P{0}=FF,00;)".format(self.codeSetID))
            return results
        finally:
            print('in finally')
            self.disconnect()

    # def setInput(self, input):
    #     try:
    #         self.connect()
    #         """Sets the input of the zone"""
    #         result = self.sendCommand(self.codeSetID, "input", input)
    #         return result
    #     finally:
    #         self.disconnect()

    def setPower(self, value):
        try:
            self.connect()
            """Sets the Power of the Zone"""
            # check power or else it times out or lower timeout?
            if value.lower() == 'on':
                return self.sendCommand("(00,S,I,{0}=40;)".format(
                                        self.codeSetID))
            else:  # else turn off
                return self.sendCommand("(00,S,I,{0}=80;)".format(
                                        self.codeSetID))
        finally:
            self.disconnect()

    # def setVolume(self, value):
    #     try:
    #         self.connect()
    #         """Sets volume of zone"""
    #         return self.sendCommand(self.codeSetID, "volume", value)
    #     finally:
    #         self.disconnect()

# all zones
activeUnits = getattr(config, 'ACTIVEUNITS')
units = {}
for unit in activeUnits:
    units[unit] = Unit(unit)
