import socket, sys, select
import common.config as config


def getCommand(zone, param):
    # zone = getattr(config, zone)
    if param.lower() == 'powerstatus':
        print(zone)
        return "(00,G, Z{0}, 24;)".format(zone)


def setCommand(zone, param, value):
    # use dic?
    # zone = getattr(config, zone)
    if param.lower() == "input":
        # need to set the input to value using config IR Command
        # val = getattr(conig, value)
        return "(00,S,I,{0},{1};)".format(zone, val)

    if param.lower() == "power":
        # need to get current power state or else it will fail
        if value.lower() == 'on':
            print("turning on")
            return "(00,S,I,{0}=40;)".format(zone)
        else:  # else turn off
            return "(00,S,I,{0}=80;)".format(zone)

    if param.lower() == "volume":
        if value.lower() == 'up':
            print("turning volume up")
            return "(00,S,I,{0},24;)".format(zone)
        else:
            return "(00,S,I,{0},C4;)".format(zone)


def parseResult(res):
    val = res.decode('UTF-8').split(',')[3]
    index = val.find('=')
    return val[index+1:index+3]


def BK600(zone, parameter, value=None):
        if value:
            command = setCommand(zone, parameter, value)
        else:
            command = getCommand(zone, parameter)
        HOST = config.HOST
        PORT = config.PORT
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.settimeout(10)
        result = ""
        try:
            s.connect((HOST, PORT))

            def readFunction():
                amount_received = 0
                amount_expected = 4

                while amount_received < amount_expected:
                    data = s.recv(50)
                    amount_received += len(data)
                    # print >>sys.stderr, 'received {0}'.format(data)
                    result = '{0}'.format(data)
                return data

            def writeFunction(command):
                print("sending {0}".format(command))
                s.send(command.encode())
                return parseResult(readFunction())

            readFunction()
            return writeFunction(command)

        finally:

            # print >>sys.stderr, 'closing socket'
            s.close()
