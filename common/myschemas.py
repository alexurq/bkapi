from marshmallow import Schema, fields


class ZoneSchema(Schema):
    zoneid = fields.Str()
    name = fields.Str()
    currentInput = fields.Str()
    powerStatus = fields.Str()
    volume = fields.Str()
    codeSetID = fields.Str()


class ZonesSchema(Schema):
    zone = fields.Nested(ZoneSchema)


class returnStats(Schema):
    zone = fields.Str()
    setCommand = fields.Str()
    value = fields.Str()


class UnitSchema(Schema):
    unit = fields.Str()
    powerStatus = fields.Str()
