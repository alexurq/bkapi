import socket
import sys
import select
import common.config as config


class Zone(object):
    def __init__(self, id):
        self.id = id
        self.codeSetID = getattr(config, id)
        self.HOST = config.HOST
        self.PORT = config.PORT
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(10)

    def connect(self):
        self.s.connect((self.HOST, self.PORT))
        """Clear the Banner"""
        banner_data = self.s.recv(50)

    def disconnect(self):
        self.s.close()

    def readFunction(self):
        amount_received = 0
        amount_expected = 4

        while amount_received < amount_expected:
            data = self.s.recv(50)
            amount_received += len(data)
            # print >>sys.stderr, 'received {0}'.format(data)
            result = '{0}'.format(data)
        return data

    def sendCommand(self, command):
        result = ""

        def parseResult(res):
            val = res.decode('UTF-8').split(',')[3]
            index = val.find('=')
            return val[index+1:index+3]

        def readFunction():
            amount_received = 0
            amount_expected = 4

            while amount_received < amount_expected:
                data = self.s.recv(50)
                amount_received += len(data)
                # print >>sys.stderr, 'received {0}'.format(data)
                result = '{0}'.format(data)
            return data

        def writeFunction(command):
            print("sending {0}".format(command))
            self.s.send(command.encode())
            return parseResult(readFunction())

        return writeFunction(command)

    def getInfo(self):
        """Returns Power Status of Zone: On/Off !not available"""
        results = {}
        try:
            self.connect()
            powerResult = self.sendCommand("(00,G, Z{0}, 24;)".format(self.codeSetID))
            if powerResult == '01':
                results['powerStatus'] = 'On'
            else:
                results['powerStatus'] = 'Off'
            results['zoneid'] = self.id
            results['codeSetID'] = self.codeSetID
            # resultInput = self.sendCommand("(00,G,P{0}=FF,02;)".format(self.codeSetID))
            # results['currentInput'] = getattr(config, "pp{0}".format(resultInput))
            results['currentInput'] = getattr(
                    config, "pp{0}".format(
                        self.sendCommand("(00,G,P{0}=FF,02;)".format(
                                         self.codeSetID))))
            volumeResult = self.sendCommand("(00,G,P{0}=FF,01;)".format(
                                            self.codeSetID))
            if volumeResult == '00':
                results['volume'] = 'MUTE'
            elif volumeResult == '28':
                results['volume'] = 'MAX'
            else:
                results['volume'] = 'In Between'
            results['name'] = self.sendCommand(
                "(00,G,P{0}=FF,00;)".format(self.codeSetID))
            return results
        finally:
            self.disconnect()

    def setInput(self, input):
        """Sets the input of the zone"""
        result = bkct600.BK600(self.codeSetID, "input", input)
        return result

    def setPower(self, value):
        """Sets the Power of the Zone"""
        return bkct600.BK600(self.codeSetID, "power", value)

    def setVolume(self, value):
        """Sets volume of zone"""
        return bkct600.BK600(self.codeSetID, "volume", value)
