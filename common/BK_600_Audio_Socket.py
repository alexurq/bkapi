import socket, sys, select

ExternalHOST = "69.23.136.27"
INTERNALHOST = "192.168.1.49"
HOST = ExternalHOST
PORT= 23
s= socket.socket(socket.AF_INET, socket.SOCK_STREAM)

s.settimeout(10)
try: 

	# test write power on
	checkPower = "(00,G,Z00,24;)"
	setUnitOff = "(00,S,Z00,24=01;)"
	setUnitOn = "(00,S,Z00,24=00;)"
	getBIKVersion = "(00,G,F4,C;)"
	getHighestBIKVersion = "(00,G,F4,11;)"
	getZ1PowerState = "(00,G,Z10,24;)" #zone specific not working
	getV2="(00,G,S,AF;)"
	setV2="(00,S,S,AF=1;)"
	setV1="(00,S,S,AF=0;)"
	getZoneInfo = "(00,G,S,64;)"
	setZPowerOn = "(00,S,Z10,24=1;)"
	setZPowerOff = "(00,S,Z10,24=0;)"
	getZoneTitle = "(00,G,ZB,0;)"
	setZoneTitle = "(00,S,ZB,0=\"BackYard\";)"
	def readFunction():
		amount_received=0
		amount_expected= 1

		while amount_received < amount_expected:
			data = s.recv(50)
			amount_received += len(data)
			#print >>sys.stderr, 'received {0}'.format(data)
			return data
	def writeFunction(command):
		print "sending {0}".format(command)
		s.send(command)
		readFunction()

	def parseResult(res):
		val = res.split(',')[3]
		index = val.find('=')
		return val[index+1:index+3]


	def getAllIDs(IDS):

		readFunction()
		validIDS = []
		for ID in IDS:
			s.send("(00,G,S,{0};)".format(ID))
			result = parseResult(readFunction())
			if result != '00':
				validIDS.append(result)
		return validIDS

	s.connect((HOST, PORT))

	#readFunction()
	#writeFunction(checkPower)
	#writeFunction(setV1)
	#writeFunction(setV2)
	#writeFunction(getV2)
	#writeFunction(setUnitOn)
	#writeFunction(getBIKVersion)
	#writeFunction(getHighestBIKVersion)
	#writeFunction(getZ1PowerState) #zone specific not working need v2.
	# writeFunction(setZPowerOn)
	#writeFunction(setZPowerOff)
	#writeFunction(setZoneTitle)
	#writeFunction(getZoneTitle)
	ids = ['64','65','66','67','68','69','6A','6B','6C','6D','6E','6F','70','71','72','73','74','75']
	print getAllIDs(ids)
finally:

	print >>sys.stderr, 'closing socket'
	s.close()

# while 1: 
# 	socket_list=[sys.stdin, s]
# 	read_sockets, write_sockets, error_sockets = select.select(socket_list, [],[])
# 	for sock in read_sockets:
# 		if sock == s:
# 			data=sock.recv(4096)
# 			if not data:
# 				print 'Connection Closed'
# 				sys.exit()
# 			else:
# 				sys.stdout.write(data)
# 		else:
# 			msg=sys.stdin.readline()
# 			s.send(msg)


