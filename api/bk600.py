"""
Support for B&K CT 600.1 Multizone.
"""
import logging
from . import bkapi

from homeassistant.components.media_player import (
    SUPPORT_TURN_OFF, SUPPORT_TURN_ON, SUPPORT_SELECT_SOURCE, MediaPlayerDevice)
from homeassistant.const import STATE_OFF, STATE_ON, CONF_HOST, CONF_NAME

# REQUIREMENTS = ['https://github.com/danieljkemp/onkyo-eiscp/archive/'
#                 'python3.zip#onkyo-eiscp==0.9.2']
_LOGGER = logging.getLogger(__name__)

SUPPORT_BK600 = SUPPORT_TURN_ON | SUPPORT_TURN_OFF | SUPPORT_SELECT_SOURCE
KNOWN_HOSTS = []
DEFAULT_SOURCES = {"FMTuner": "FMTuner", "AMTuner": "AMTuner", "Dedicated": "Dedicated", "IN1": "IN1", "IN2": "IN2", "IN3": "IN3", "IN4": "IN4", "IN5": "IN5", "IN6": "IN6", "IN7": "IN7", "IN8": "IN8", "IN9": "IN9"}
CONFIG_SOURCE_LIST = "sources"


def setup_platform(hass, config, add_devices, discovery_info=None):
    """Setup the B&K platform."""
    zones=config.get('zones', {})
    devices = []
    if discovery_info is not None:
        host = discovery_info[0]
        port = 23
    else:
        host = config.get(CONF_HOST)
        port = int(config.get('port', 23))

    if not host:
        _LOGGER.error(
            "Missing required configuration items in %s: %s",
            DOMAIN,
            CONF_HOST)
        return False

    # Only add a media server once
    if host in KNOWN_HOSTS:
        return False
    KNOWN_HOSTS.append(host)
    bk = bkapi.BKCT600_1(
        host, port)

    if not bk.init_success:
        return False
    sources = config.get(CONFIG_SOURCE_LIST, DEFAULT_SOURCES)
    for dev_name, properties in zones.items():
        devices.append(BK600Zones(
            bk,
            str(dev_name),
            properties.get('name'),
            sources
            )
        )
    add_devices(devices)

    return True


# pylint: disable=too-many-instance-attributes
class BK600Zones(MediaPlayerDevice):
    """Representation of a BK600 zone device."""

    # pylint: disable=too-many-public-methods, abstract-method
    def __init__(self, bk, id, name, sources):
        """Initialize the BK600 Receiver."""
        self._bk = bk
        self._id = id
        self._name = name
        self._current_source = None
        self._pwstate = STATE_OFF
        self._source_list = list(sources.values())
        self._source_mapping = sources
        self._reverse_mapping = {value: key for key, value in sources.items()}
        self.update()

    def update(self):
        """Get the latest details from the device."""
        status = self._bk.getInfo(self._id)
        print(status)
        if status.get('powerStatus') == 'ON':
            self._pwstate = STATE_ON
        else:
            self._pwstate = STATE_OFF
            return
        current_source_raw = status.get('currentInput')
        if current_source_raw in self._source_mapping:
            # print(source)
            self._current_source = self._source_mapping[current_source_raw]
        else:
            self._current_source = current_source_raw

    @property
    def name(self):
        """Return the name of the device."""
        return self._name

    @property
    def state(self):
        """Return the state of the device."""
        return self._pwstate

    # @property
    # def volume_level(self):
    #     """Volume level of the media player (0..1)."""
    #     return self._volume

    # @property
    # def is_volume_muted(self):
    #     """Boolean if volume is currently muted."""
    #     return self._muted

    @property
    def supported_media_commands(self):
        """Flag of media commands that are supported."""
        return SUPPORT_BK600

    @property
    def source(self):
        """"Return the current input source of the device."""
        return self._current_source

    @property
    def source_list(self):
        """List of available input sources."""
        return self._source_list

    def turn_off(self):
        """Turn off media player."""
        self._bk.power_off(self._id)

    # def set_volume_level(self, volume):
    #     """Set volume level, input is range 0..1. Onkyo ranges from 1-80."""
    #     self._bk.command('volume {}'.format(int(volume*80)))

    # def mute_volume(self, mute):
    #     """Mute (true) or unmute (false) media player."""
    #     if mute:
    #         self._bk.command('audio-muting on')
    #     else:
    #         self._bk.command('audio-muting off')

    def turn_on(self):
        """Turn the media player on."""
        self._bk.power_on(self._id)

    def select_source(self, source):
        """Set the input source."""
        if source in self._source_list:
            source = self._reverse_mapping[source]
        self._bk.setInput(self._id, source)
