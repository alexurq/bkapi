# HOST = "69.23.136.27"
HOST = "192.168.1.49"
PORT = 23
ACTIVEZONES = ["F", "E"]
ACTIVEUNITS = ["00"]

# IR Params to set inputs of zone
# command ex: (00,S,I,Z=hex;)
#us  Preset instead (00,S,P{}=FF,3=hex;)
FM = '0'
AM = '1'
Dedicated = '2'
# IN1 = '70'
IN1 = '3'
IN2 = '4'
IN3 = '5'
IN4 = '6'
IN5 = '7'
IN6 = '8'
IN7 = '9'
IN8 = 'A'
IN9 = 'B'
# IR PowerStates
off = '80'
on = '40'

# Zones - logical to group id hex ex: A(ID of 11, 11 hex is B)
A = 'B'
B = 'C'
C = 'D'
D = 'E'
E = 'F'
F = '10'

# Preset parameters
pp00 = "FMTuner"
pp01 = 'AMTuner'
pp02 = 'Dedicated'
pp03 = 'IN1'
pp04 = 'IN2'
pp05 = 'IN3'
pp06 = 'IN4'
pp07 = 'IN5'
pp08 = 'IN6'
pp09 = 'IN7'
pp0A = 'IN8'
pp0B = 'IN9'

#Zone Specifi
#power State Commands
#(00,X,2,1=1;) = Turn zone 1 on
#(00,X,2,2=0;) = turn zone 2 off
# Zone specific power states
#24 == zone power state
# (00, G, Z1, 24;) get Z1 power
#1 = power on, 0 = power off


#----------Code Set IDs -------
#could query device for valid code sets - would have to query each available
#group that the device supports - CT.600.1 is a-r with hex
#64,65,66,67,68,69,6A,6B,6C,6D,6E,6F,70,71,72,73,74,75
#once we have ID we could use code set ID and zone specific command
#(00,G,ZB,0;) to get title
