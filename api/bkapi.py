import socket
import sys
import select
# from . import config

OUT_INPUT_PARAMS = {"00": "FMTuner", "01": "AMTuner", "02": "Dedicated",
    "03": "IN1", "04": "IN2", "05": "IN3", "06": "IN4", "07": "IN5",
    "08": "IN6", "09": "IN7", "0A": "IN8", "0B": "IN9"}
IN_INPUT_PARAMS = {value: key for key, value in OUT_INPUT_PARAMS.items()}
ZONES = {"A": "B", "B": "C", "C": "D", "D": "E", "E": "F", "F": "10"}

class BKCT600_1:

    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        # self.zones = self._getZones()
        self.connected = self.connect()
        self.init_success = True if self.connected else False



    def _getZones(self):
        """Return all of the active zones on the BK600"""
        self.connect()
        command = "(0,G,F4,12;)"
        # print("sending {0}".format(command))
        self.s.send(command.encode())
        results = self.readFunction()
        val = results.split(',')[3]
        start = val.find('=')
        end = val.find(';')
        zones = val[start+2:end-1].split(" ")
        # print (zones)
        return zones


    def connect(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.s.settimeout(10)
            self.s.connect((self.ip, self.port))
            """Clear the Banner"""
            banner_data = self.s.recv(50)
            return True
        except:
            return False

    def disconnect(self):
        self.s.close()

    def emptySocketBuffer(self):
        self.s.settimeout(0.0)
        while 1:
            try:
                data = self.s.recv(150)
                # print("data left in buffer:{0}".format(data))
            except:
                break
        self.s.settimeout(10)

    def parseResult(self, res):
        # because of the read function decoded the data already
        # val = res.decode('UTF-8').split(',')[3]
        val = res.split(',')[3]
        # print(val)
        index = val.find('=')
        return val[index+1:index+3]


    def readFunction(self):
        amount_received = 0
        amount_expected = 150

        # # while amount_received < amount_expected:
        # data = self.s.recv(50)
        # # amount_received += len(data)
        # # print >>sys.stderr, 'received {0}'.format(data)
        # result = '{0}'.format(data)
        # return data
        End = ')'
        total_data = []
        data = ''
        while True:
            data = self.s.recv(8192).decode('UTF-8')
            if End in data:
                total_data.append(data[:data.find(End)])
                break
            total_data.append(data)
            if len(total_data) > 1:
                # check if end_of_data was split
                last_pair = total_data[-2]+total_data[-1]
                if End in last_pair:
                    total_data[-2] = last_pair[:last_pair.find(End)]
                    total_data.pop()
                    break
        return ''.join(total_data)


    def sendCommand(self, command):
        # print("sending {0}".format(command))
        self.emptySocketBuffer()
        self.s.send(command.encode())
        return self.parseResult(self.readFunction())


    def getInfo(self, zone):
        """Returns Power Status of Zone: On/Off !not available"""
        results = {}
        zone = ZONES.get(zone)

        powerResult = self.sendCommand("(00,G, Z{0}, 24;)".format(
                                       zone))
        if powerResult == '01':
            results['powerStatus'] = 'ON'
        else:
            results['powerStatus'] = 'OFF'
        # resultInput = self.sendCommand("(00,G,P{0}=FF,02;)".format(self.codeSetID))
        # results['currentInput'] = getattr(config, "pp{0}".format(resultInput))
        ciRes = self.sendCommand("(00,G,P{0}=FF,03;)".format(zone))
        # print(ciRes)
        results['currentInput'] = OUT_INPUT_PARAMS.get(ciRes)
        # volumeResult = self.sendCommand("(00,G,P{0}=FF,01;)".format(
        #                                 zone))
        # if volumeResult == '00':
        #     results['volume'] = 'MUTE'
        # elif volumeResult == '28':
        #     results['volume'] = 'MAX'
        # else:
        #     results['volume'] = 'In Between'
        # results['name'] = self.sendCommand(
        #     "(00,G,P{0}=FF,00;)".format(zone))
        return results

    def setInput(self, zone, input):
        """Sets the input of the zone"""
        # result = self.sendCommand(self.codeSetID, "input", input)
        # print("(00,S,P{0}=FF,3={1};)".format(
        #         getattr(config, zone), getattr(config, input)))
        result = self.sendCommand("(00,S,P{0}=FF,3={1};)".format(
                                    ZONES.get(zone), IN_INPUT_PARAMS.get(input)))
        return result


    def power_on(self, zone):
        """Sets the Power of the Zone"""
        res =  self.sendCommand("(00,S,I,{0}=40;)".format(
                                    ZONES.get(zone)))
        return res

    def power_off(self, zone):
        res = self.sendCommand("(00,S,I,{0}=80;)".format(
                                        ZONES.get(zone)))
        return res

    def setVolume(self, zone, value):
        """Sets volume of zone"""
        return self.sendCommand(ZONES.get(zone), "volume", value)


