from flask_restful import Resource
from flask import request
from common.myschemas import UnitSchema, returnStats
import common.units as units


class Unit(Resource):
    def get(self, unit_id):
        unitschema = UnitSchema()
        # zone = Zone(zone_id)
        #unit = units.unit[str(unit_id)]
        unit = units.Unit(unit_id)
        # var  = getattr(classes, zonesObj)
        command = request.args.get('command')
        result = unitschema.dump(unit.getInfo())
        return result.data

    def post(self, unit_id):
        unit = units.unit[unit_id]
        command = request.args.get('command')
        value = request.args.get('value')
        if command:
            if command.lower() == 'power':
                return unit.setPower(value)
            # elif command.lower() == 'changeinput':
            #     return unit.setInput(value)
            # elif command.lower() == 'volume':
            #     return unit.setVolume(value)
            else:
                return "Command not found"
        else:
            return "No Command Given"
