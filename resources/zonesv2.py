from flask_restful import Resource
from flask import request
from common.myschemas import ZoneSchema, ZonesSchema, returnStats
# from common.zoneclass import Zone
import common.zones as zones


class Zones(Resource):
	def get(self, zone_id):
		zoneschema = ZoneSchema()
		# zone = Zone(zone_id)
		zone = zones.zones[zone_id]
		# var  = getattr(classes, zonesObj)
		command = request.args.get('command')
		result = zoneschema.dump(zone.getInfo())
		return result.data

	def put(self, zone_id):
		zone = zones.zones[zone_id]
		command = request.args.get('command')
		value = request.args.get('value')
		if command.lower() == 'power':
			return zone.setPower(value)
		elif command.lower() == 'changeinput':
			return zone.setInput(value)
		elif command.lower() == 'volume':
			return zone.setVolume(value)
		else:
			return "Command not found"

	def post(self, zone_id):
		zone = zones.zones[zone_id]
		command = request.args.get('command')
		value = request.args.get('value')
		if command:
			if command.lower() == 'power':
				return zone.setPower(value)
			elif command.lower() == 'changeinput':
				return zone.setInput(value)
			elif command.lower() == 'volume':
				return zone.setVolume(value)
			else:
				return "Command not found"
		else:
			return "No Command Given"
		# schema = returnStats()
		# result = schema.dump(obj)
		# return result.data
		# return res
