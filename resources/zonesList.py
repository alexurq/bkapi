from flask_restful import Resource
from flask import request
from common.myschemas import ZoneSchema
from common.classes import zones

class ZonesList(Resource):
	def get(self):
		zoneschema = ZoneSchema(many=True)
		# var = getattr(classes, zones)
		result = zoneschema.dump(zones)
		return result.data
